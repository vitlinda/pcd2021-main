package pcd.ass02.es3;

import java.util.HashMap;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class WordStreamBuilder  {

	private Flag stopFlag;
	private HashMap<String,String> wordsToDiscard;
	
	public WordStreamBuilder (HashMap<String,String> wordsToDiscard, Flag stopFlag) {
		this.stopFlag = stopFlag;
		this.wordsToDiscard = wordsToDiscard;
	}

	public Observable<String> createWordStream(Observable<String> source) {
		return source
			.observeOn(Schedulers.computation()) 
			.flatMap((String chunk) -> {
				String del = "[\\x{201D}\\x{201C}\\s'\", ?.@;:!-]+";
				return Observable.create(emitter -> {
							if (!stopFlag.isSet()) {
								String[] words = chunk.split(del);
								for (String w : words) {
									String w1 = w.trim().toLowerCase();
									if (!w1.equals("") && !wordsToDiscard.containsKey(w1)) {
										emitter.onNext(w1);
									}
								}
							} else {
								logDebug("stopped");
							}
							emitter.onComplete();
					    });
		});

	}
	

	protected void log(String msg) {
		System.out.println("[ Text Analyser Stream ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Text Analyser Stream ] " + msg);
	}
	
}
