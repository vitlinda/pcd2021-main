package pcd.ass02.es1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

import pcd.ass01.sol.part1.BasicAgent;


public class Master extends BasicAgent {

	private File configFile;
	private File dir;
	private int numMostFreqWords;
	
	private HashMap<String,String> wordsToDiscard;
	private WordFreqMap map;
	
	private Flag stopFlag;
	
	private View view;
	
	/* performance tuning params */
	
	private ExecutorService loaderDocExec;
	private ExecutorService textAnalyzExec;
	private ExecutorService docDiscoverExec;	
	public Master(File configFile, File dir, int numMostFreqWords, Flag stopFlag, View view) {
		super("master");
		this.configFile = configFile;
		this.dir = dir;
		this.numMostFreqWords = numMostFreqWords;
		this.view = view;
		this.stopFlag = stopFlag;
	}
	
	public void run() {
		log("started.");
		try {
			int nProcs = Runtime.getRuntime().availableProcessors();
			
			docDiscoverExec = Executors.newSingleThreadExecutor();
			loaderDocExec = Executors.newCachedThreadPool();
			textAnalyzExec = Executors.newFixedThreadPool(nProcs);
			
			long t0 = System.currentTimeMillis();
			
			map = new WordFreqMap(numMostFreqWords);

			Flag done = new Flag();
			Viewer viewer = new Viewer(map,view,done);
			viewer.start();
			
			loadWordsToDiscard(configFile);

			SharedData sharedData = new SharedData(map, wordsToDiscard, stopFlag, loaderDocExec, textAnalyzExec);
			
			/* launch tasks */
			
			Future<List<Future<List<Future<Void>>>>> f = docDiscoverExec.submit(new DocDiscoveryTask(dir, sharedData));

			List<Future<List<Future<Void>>>> futs = f.get();
			for (Future<List<Future<Void>>> f2: futs) {
				List<Future<Void>> flist = f2.get();
				for (Future<Void> f4: flist) {
					f4.get();
				}
			}
			log("all tasks completed.");
						
			long t2 = System.currentTimeMillis();
			done.set();
			view.done();
			
			elabMostFreqWords();
			
			log("done in " + (t2-t0));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
		
	private void loadWordsToDiscard(File configFile) {
		try {
			wordsToDiscard = new HashMap<String,String>();
			FileReader fr = new FileReader(configFile);
			BufferedReader br = new BufferedReader(fr);
			br.lines().forEach(w -> {
				wordsToDiscard.put(w, w);
			});			
			fr.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
	}	
	
	private void elabMostFreqWords() {
		Object[] freqs = map.getCurrentMostFreq();
		for (int i = numMostFreqWords-1; i >=0; i--) {
			AbstractMap.SimpleEntry<String, Integer> el = (AbstractMap.SimpleEntry<String, Integer>) freqs[i];
			String key = el.getKey();
			System.out.println(" " + (i+1) + " - " +  key + " " + el.getValue());
		}		
	}

}
