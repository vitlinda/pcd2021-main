package pcd.ass02.es2.first;

import java.time.LocalTime;
import java.util.Optional;

public class TrainData {

	private String trainId;
	// departure
	private TrainStopData origin;
	
	// arrivals
	private TrainStopData lastStop;
	private TrainStopData destination;
		
	private String lastDetectionPlace;
	private LocalTime lastDetectionTime;
	private long currentDelayInMin;
	
	private LocalTime snapshotTime;
	
	public TrainData(String trainId, LocalTime snapshotTime) {
		this.trainId = trainId;
		this.snapshotTime = snapshotTime;
	}
	
	public String getTrainId() {
		return trainId;
	}
	
	public LocalTime getSnapshotTime() {
		return this.snapshotTime;
	}
	
	public void setOrigin(TrainStopData data) {
		this.origin = data;
	}

	public void setDestination(TrainStopData data) {
		this.destination = data;
	}

	public void setLastStop(TrainStopData data) {
		this.lastStop = data;
	}
	
	public void setLastDetectionInfo(String place, LocalTime time, long delay) {
		this.lastDetectionPlace = place;
		this.lastDetectionTime = time;
		this.currentDelayInMin = delay;
	}
	
	public TrainStopData getOrigin() {
		return origin;
	}

	public TrainStopData getDestination() {
		return destination;
	}
	
	public Optional<TrainStopData> getLastStop() {
		return lastStop != null ? Optional.of(lastStop) : Optional.empty();
	}
	
	public Optional<String> getLastDetectionPlace() {
		return (this.lastDetectionPlace != null && !this.lastDetectionPlace.equals("")) ? Optional.of(this.lastDetectionPlace) : Optional.empty();
	}

	public Optional<LocalTime> getLastDetectionTime() {
		return this.lastDetectionTime != null ? Optional.of(this.lastDetectionTime) : Optional.empty();
	}
	
	public long getLastDetectionDelayInMins() {
		return this.currentDelayInMin;
	}
	
	public boolean isTravelling() {
		return this.isAlreadyDeparted() && !this.isAlreadyArrived();
	}
	
	public boolean isAlreadyDeparted() {
		return this.origin != null && !this.origin.isPrediction();		
	}
	
	public boolean isAlreadyArrived() {
		return this.destination != null && !this.destination.isPrediction();		
	}

}
