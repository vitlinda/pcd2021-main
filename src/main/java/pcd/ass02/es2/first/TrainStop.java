package pcd.ass02.es2.first;

import java.time.LocalDateTime;
import java.util.Optional;

public class TrainStop {

	private String stationName;
	private Optional<LocalDateTime> arrTime;
	private Optional<LocalDateTime> depTime;

	public TrainStop(String stationName, Optional<LocalDateTime> arrTime, Optional<LocalDateTime> depTime) {
		super();
		this.stationName = stationName;
		this.arrTime = arrTime;
		this.depTime = depTime;
	}
	
	public String getStationName() {
		return stationName;
	}

	public Optional<LocalDateTime> getArrTime() {
		return arrTime;
	}

	public Optional<LocalDateTime> getDepTime() {
		return depTime;
	}
}
