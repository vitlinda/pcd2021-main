package pcd.ass02.es2.first;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class GetTrainDataExample {
	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		TrainLib tl = new TrainLib(vertx);
		
		Future<TrainData> res = tl.getRealTimeTrainData("3904");
		res.onSuccess(s -> {
			dump("train id: " + s.getTrainId());
			dump(" origin: " + s.getOrigin());
			dump(" destination: " + s.getDestination());
			dump(" last stop: " + (s.getLastStop().isPresent() ? s.getLastStop().get() : "- "));
			dump(" last detect stat: " + (s.getLastDetectionPlace().isPresent() ? s.getLastDetectionPlace().get() : "-") + " at " + (s.getLastDetectionTime().isPresent() ? s.getLastDetectionTime().get() : "-"));
			
		});
	}
	
	private static void dump(String st) {
		System.out.println(st);
	}

}
