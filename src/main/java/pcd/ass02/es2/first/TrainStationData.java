package pcd.ass02.es2.first;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class TrainStationData {

	private List<Departure> departures;
	private List<Arrival> arrivals;
	private String stationId;
	private LocalTime time;
	
	public TrainStationData(String id, LocalTime time) {
		departures = new ArrayList<Departure>();
		arrivals = new ArrayList<Arrival>();
		this.stationId = id;
		this.time = time;
	}
	
	public void addDeparture(Departure d) {
		departures.add(d);
	}

	public void addArrival(Arrival a) {
		arrivals.add(a);
	}
	
	public List<Departure> getDepartures(){
		return departures;
	}

	public List<Arrival> getArrivals(){
		return arrivals;
	}
	
	public String getStationId() {
		return stationId;
	}
	
	public LocalTime getTime() {
		return time;
	}
}
