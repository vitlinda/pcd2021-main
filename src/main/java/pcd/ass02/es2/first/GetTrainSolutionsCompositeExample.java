package pcd.ass02.es2.first;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class GetTrainSolutionsCompositeExample {

	public static void main(String[] args) throws Exception {

		
		Vertx vertx = Vertx.vertx();
		TrainLib tl = new TrainLib(vertx);
		
		// Future<List<TravelSolution>> fut = tl.getTrainSolutions("CESENA","BOLOGNA%20CENTRALE","06/06/2021","07");
		Future<List<TravelSolution>> fut = tl.getTrainSolutions("CESENA","MILANO%20CENTRALE","06/06/2021","14");
		fut.onSuccess(list -> {
			for (Object o: list) {
				TravelSolution sol = (TravelSolution) o;
				dump(sol.getDepStation()+" at " + sol.getDepTime() 
					+ " -> " + sol.getArrStation() + " at " + sol.getArrTime());

				List<Train> trains = sol.getTrains();
				for (Train t: trains) {
					dump(" " + t.getId() + " - " + t.getStopList().size());
					for (TrainStop st: t.getStopList()) {
						dump("   " + st.getStationName() + " - arr: " + (st.getArrTime().isPresent() ? st.getArrTime().get() : "-") + " - dep: " + (st.getDepTime().isPresent() ? st.getDepTime().get() : "-")); 
					}
				}
			}
			TravelSolution sol = list.get(0);
			dump("FIRST SOLUTION - DUMP: " + sol.getDepTime());			
			List<Train> trains = sol.getTrains();
			List<Future> allFutures = new ArrayList<Future>();
			for (Train t: trains) {	
				Future<TrainData> res = tl.getRealTimeTrainData(t.getNumericId());
				allFutures.add(res);
			}
			
			CompositeFuture
				.all(allFutures)
				.onSuccess(cf -> {
					List<TrainData> tdList = cf.result().list();
					for (TrainData s: tdList) {
						dump(s.getTrainId());
						dump(" origin: " + s.getOrigin());
						dump(" destination: " + s.getDestination());
						dump(" last stop: " + (s.getLastStop().isPresent() ? s.getLastStop().get() : "- "));
						dump(" last detect stat: " + (s.getLastDetectionPlace().isPresent() ? s.getLastDetectionPlace().get() : "-") + " at " + (s.getLastDetectionTime().isPresent() ? s.getLastDetectionTime().get() : "-"));
						
					}
				});
			
		}).onFailure(h -> {
			dump("failure: " + h);
		});
	}
	
	private static void dump(String st) {
		System.out.println(st);
	}

}
