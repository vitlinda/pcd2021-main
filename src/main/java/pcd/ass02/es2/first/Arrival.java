package pcd.ass02.es2.first;

import java.time.LocalTime;

public class Arrival {

	private String trainId;
	private String dest;
	private LocalTime time;
	
	public Arrival(String trainId, String dest, LocalTime time) {
		this.dest = dest;
		this.time = time;
		this.trainId = trainId;
	}

	public String getDest() {
		return dest;
	}

	public LocalTime getTime() {
		return time;
	}

	public String getTrainId() {
		return trainId;
	}
	
}
