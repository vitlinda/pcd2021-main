package pcd.ass02.es2.first;

import java.util.ArrayList;
import java.util.List;

public class Train {

	private String id;
	private List<TrainStop> stopList;

	public Train(String id) {
		this.id = id;
		stopList = new ArrayList<TrainStop>();
	}
	
	public String getId() {
		return id;
	}

	public String getNumericId() {
		String[] toks = id.split(" ");
		return toks[toks.length - 1];
	}
	
	public void addTrainStop(TrainStop ts) {
		stopList.add(ts);
	}

	public List<TrainStop> getStopList(){
		return stopList;
	}
	
	
}
