package pcd.ass02.es2.first;

import java.util.List;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class GetTrainSolutionsExample {

	public static void main(String[] args) throws Exception {

		
		Vertx vertx = Vertx.vertx();
		TrainLib tl = new TrainLib(vertx);
		
		Future<List<TravelSolution>> fut = tl.getTrainSolutions("CESENA","BOLOGNA%20CENTRALE","06/06/2021","07");
		fut.onSuccess(list -> {
			for (Object o: list) {
				TravelSolution sol = (TravelSolution) o;
				dump(sol.getDepStation()+" at " + sol.getDepTime() 
					+ "-> " + sol.getArrStation() + " at " + sol.getArrTime());

				List<Train> trains = sol.getTrains();
				for (Train t: trains) {
					dump(" " + t.getId() + " - " + t.getStopList().size());
					for (TrainStop st: t.getStopList()) {
						dump("   " + st.getStationName() + " - ar: " + (st.getArrTime().isPresent() ? st.getArrTime().get() : "-") + " - de: " + (st.getDepTime().isPresent() ? st.getDepTime().get() : "-")); 
					}
				}
			}
		}).onFailure(h -> {
			dump("failure: " + h);
		});
	}
	
	private static void dump(String st) {
		System.out.println(st);
	}

}
