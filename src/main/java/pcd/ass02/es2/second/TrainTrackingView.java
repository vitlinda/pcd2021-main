package pcd.ass02.es2.second;

import java.util.List;

import pcd.ass02.es2.first.TrainData;
import pcd.ass02.es2.first.TravelSolution;

/**
 * 
 * View designed as a monitor.
 * 
 * @author aricci
 *
 */
public class TrainTrackingView {

	private TrainTrackingGUI gui;
	
	public TrainTrackingView(Controller contr){	
		gui = new TrainTrackingGUI(contr);
	}
	
	public void updateSolutions(List<TravelSolution> solutions) {
		gui.updateSolutions(solutions);
	}
	
	public void updateSolutionState(List<TrainData> state) {
		gui.updateSolutionState(state);
	}

	public void startTracking(TravelSolution sol) {
		gui.startTracking(sol);
	}
	
	public void stopTracking() {
		gui.stopTracking();
	}
	
	public void resetSolutions() {
		gui.resetSolutions();
	}
	
	public void display() {
		gui.display();
    }
}
