package pcd.ass02.es2.second;

import javax.swing.*;

import pcd.ass02.es2.first.Train;
import pcd.ass02.es2.first.TrainData;
import pcd.ass02.es2.first.TrainStop;
import pcd.ass02.es2.first.TravelSolution;

import java.awt.*;
import java.awt.event.*;
import java.time.LocalDateTime;
import java.util.List;

public class TrainTrackingGUI extends JFrame implements ActionListener {

	private JTextField from, to;
	private JTextField date, time;
	private JButton find, track, stop;
	private JComboBox<String> solutions;
	private JTextArea trackedInfo, solutionInfo;
	private JTextField state;
	private Box findBox, solutionsBox, trackBox;

	private Controller controller;
	
	public TrainTrackingGUI(Controller contr){
		setTitle("Train tracking GUI");
		setSize(700,700);		
		controller = contr;
		
		Container cp = getContentPane();
		JPanel panel = new JPanel();
		
		findBox = new Box(BoxLayout.X_AXIS);
		
		Box b0 = new Box(BoxLayout.Y_AXIS);
		Box b00 = new Box(BoxLayout.X_AXIS);
		from = new JTextField(10);
		from.setText("CESENA");
		to = new JTextField(10);
		to.setText("MILANO CENTRALE");
		b00.add(new JLabel("From"));
		b00.add(from);
		b00.add(new JLabel("To"));
		b00.add(to);
		Box b01 = new Box(BoxLayout.X_AXIS);
		date = new JTextField(10);
		date.setText("07/06/2021");
		time = new JTextField(6);
		time.setText("07");
		b01.add(new JLabel("Date"));
		b01.add(date);
		b01.add(new JLabel("Time"));
		b01.add(time);
		b0.add(b00);
		b0.add(b01);
		
		findBox.add(b0);
		find = new JButton("Find");
		find.addActionListener(this);
		findBox.add(find);

		solutionsBox = new Box(BoxLayout.X_AXIS);
		// solutionsBox.setEnabled(false);
		solutions = new JComboBox<String>();
		// solutions.setEnabled(false);
		solutionsBox.add(new JLabel("Solutions"));
		solutionsBox.add(solutions);

		
		Box b1 = new Box(BoxLayout.Y_AXIS);
		track = new JButton("Track");
		track.addActionListener(this);
		// track.setEnabled(false);
		b1.add(track);
		stop  = new JButton("stop");
		stop.setEnabled(false);
		stop.addActionListener(this);
		b1.add(stop);		
		solutionsBox.add(b1);


		Box solutionBox = new Box(BoxLayout.X_AXIS);
		solutionInfo = new JTextArea(12,45);
		JScrollPane pane1 = new JScrollPane(solutionInfo,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		solutionInfo.setEnabled(false);
		solutionBox.add(pane1);

		Box trackBox = new Box(BoxLayout.X_AXIS);
		trackedInfo = new JTextArea(12,45);
		JScrollPane pane2 = new JScrollPane(trackedInfo,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		trackBox.add(pane2);

		Box stateBox = new Box(BoxLayout.X_AXIS);
		state = new JTextField(10);
		state.setEditable(false);
		state.setText("Idle.");
		stateBox.add(state);

		Box p = new Box(BoxLayout.Y_AXIS);
		p.add(findBox);
		p.add(Box.createVerticalStrut(10));
		p.add(solutionsBox);
		p.add(Box.createVerticalStrut(10));
		
		Box bb1 = new Box(BoxLayout.X_AXIS);
		bb1.add(new JLabel("Selected solution - General Info"));
		bb1.add(Box.createHorizontalStrut(380));
		p.add(bb1);
		p.add(Box.createVerticalStrut(5));
		p.add(solutionBox);
		p.add(Box.createVerticalStrut(10));
		Box bb2 = new Box(BoxLayout.X_AXIS);
		bb2.add(new JLabel("Selected Solution - Real Time info"));
		bb2.add(Box.createHorizontalStrut(375));
		p.add(bb2);
		p.add(Box.createVerticalStrut(5));
		p.add(trackBox);
		p.add(Box.createVerticalStrut(10));
		p.add(stateBox);
				
		panel.add(p);
		cp.add(panel);
		
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent ev){
				System.exit(-1);
			}
			public void windowClosed(WindowEvent ev){
				System.exit(-1);
			}
		});
	}
	
	public void actionPerformed(ActionEvent ev){
		Object src = ev.getSource();
		if (src == find) {
			state.setText("Finding solutions...");
			controller.notifySolutionToFind(from.getText(), to.getText(), date.getText(), time.getText());
		} else if (src == track) {
			state.setText("Tracking...");
			controller.notifySolutionToTrack(solutions.getSelectedIndex());
		} else if (src == stop) {
			state.setText("Idle.");
			controller.notifyStopTracking();
		}
	}
	
	public void display() {
        javax.swing.SwingUtilities.invokeLater(() -> {
        	this.setVisible(true);
        });
    }

	public void stopTracking() {
		javax.swing.SwingUtilities.invokeLater(() -> {
			trackedInfo.setEnabled(false);
        	track.setEnabled(true);
        	stop.setEnabled(false);
        	find.setEnabled(true);        	
		});
	}
	
	public void startTracking(TravelSolution sol) {
		javax.swing.SwingUtilities.invokeLater(() -> {
			solutionInfo.setEnabled(true);			
			trackedInfo.setEnabled(true);
        	track.setEnabled(false);
        	find.setEnabled(false);        	
        	stop.setEnabled(true);
        	StringBuffer buf = new StringBuffer();
    		for (Train t: sol.getTrains()) {
				buf.append("TRAIN: " + t.getId() + "\n");
				for (TrainStop st: t.getStopList()) {
					buf.append("   " + st.getStationName() + " - arr: " + (st.getArrTime().isPresent() ? st.getArrTime().get() : "-") + " - dep: " + (st.getDepTime().isPresent() ? st.getDepTime().get() : "-") + "\n"); 
				}   			
    		}
        	this.solutionInfo.setText(buf.toString());
		});
	}

	public void resetSolutions() {
		javax.swing.SwingUtilities.invokeLater(() -> {
			MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<String>();
			solutions.setModel(comboModel);
        });
	}

	public void updateSolutions(List<TravelSolution> sol) {
		
		javax.swing.SwingUtilities.invokeLater(() -> {
			MutableComboBoxModel<String> comboModel = new DefaultComboBoxModel<String>();
			for (TravelSolution ts: sol) {
				LocalDateTime dep = ts.getDepTime();
				LocalDateTime arr = ts.getArrTime();
        		comboModel.addElement("Leaving at " + dep  + " - arrival at " + arr);
        	}
			solutions.setModel(comboModel);
        	solutions.setEnabled(true);
        	track.setEnabled(true);
        	state.setText("Idle.");
        });
	}

	public void updateSolutionState(List<TrainData> state) {
		StringBuffer buf = new StringBuffer();
		javax.swing.SwingUtilities.invokeLater(() -> {
			for (TrainData s: state) {
				buf.append(s.getTrainId() + " at " + s.getSnapshotTime() + "\n");
				buf.append(" origin: " + s.getOrigin()+ "\n");
				buf.append(" destination: " + s.getDestination() + "\n");
				buf.append(" last stop: " + (s.getLastStop().isPresent() ? s.getLastStop().get() : "- ") + "\n");
				buf.append(" last detect stat: " + (s.getLastDetectionPlace().isPresent() ? s.getLastDetectionPlace().get() : "-") + " at " + (s.getLastDetectionTime().isPresent() ? s.getLastDetectionTime().get() : "-") + "\n");
			}
			this.trackedInfo.setText(buf.toString());
		});
	}

	
	
}
