package pcd.lab03.sem.ex;

import java.util.concurrent.Semaphore;

/**
 * Synchronized version
 * 
 *
 * @author linda
 *
 */
public class TestPingPong {
	public static void main(String[] args) {
		Semaphore eventPing = new Semaphore(0);
		Semaphore eventPong = new Semaphore(0);

		new Pinger("Pinger", eventPing, eventPong).start();
		new Ponger("Ponger", eventPing, eventPong).start();
	}

}
