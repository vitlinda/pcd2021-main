package pcd.lab03.sem.ex;

import java.util.concurrent.Semaphore;

public class Pinger extends Thread {

	private Semaphore eventPonger;
	private Semaphore eventPinger;

	public Pinger(String name, Semaphore eventPinger, Semaphore eventPonger) {
		super(name);
		this.eventPonger = eventPonger;
		this.eventPinger = eventPinger;
	}
	
	public void run() {
		while (true) {
			try {
				System.out.println("ping!");
				this.eventPinger.release();
				this.eventPonger.acquire();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}