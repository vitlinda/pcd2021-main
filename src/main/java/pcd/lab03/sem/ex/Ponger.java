package pcd.lab03.sem.ex;

import java.util.concurrent.Semaphore;

public class Ponger extends Thread {

	private Semaphore eventPonger;
	private Semaphore eventPinger;

	public Ponger(String name, Semaphore eventPinger, Semaphore eventPonger) {
		super(name);
		this.eventPonger = eventPonger;
		this.eventPinger = eventPinger;
	}	
	
	public void run() {
		while (true) {
			try {
				eventPinger.acquire();
				System.out.println("pong!");
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				eventPonger.release();
			}
		}
	}
}