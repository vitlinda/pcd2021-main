package pcd.lab03.monitors.barrier;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
